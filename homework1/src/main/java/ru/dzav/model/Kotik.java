package ru.dzav.model;

import java.util.Objects;
import java.util.UUID;

public class Kotik {

    private static final int DEFAULT_FULLNESS_VALUE = 10;

    private static int amountOfCats = 0;

    private String kotikUUID;
    private String name;
    private int weight;
    private int fullness;
    private int prettiness;
    private String meow;

    public Kotik(String name, int weight, int prettiness, String meow) {
        this.kotikUUID = UUID.randomUUID().toString();
        this.name = name;
        this.weight = weight;
        this.prettiness = prettiness;
        this.meow = meow;
        this.fullness = DEFAULT_FULLNESS_VALUE;
        increaseCounterOfCats();
    }

    public Kotik() {
        this.kotikUUID = UUID.randomUUID().toString();
        fullness = DEFAULT_FULLNESS_VALUE;
        increaseCounterOfCats();
    }

    public String getKotikUUID() {
        return kotikUUID;
    }

    public void setKotikUUID(String kotikUUID) {
        this.kotikUUID = kotikUUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getFullness() {
        return fullness;
    }

    public void setFullness(int fullness) {
        this.fullness = fullness;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public static int getAmountOfCats() {
        return amountOfCats;
    }

    @Override
    public String toString() {
        return "Kotik with name - " + name + "; weight - " + weight + "; prettiness - " + prettiness + "; voice sounds like - " + meow + "; fullness - " + fullness + ";";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kotik kotik = (Kotik) o;
        return meow.equals(kotik.meow);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meow);
    }

    private static void increaseCounterOfCats() {
        ++amountOfCats;
    }
}

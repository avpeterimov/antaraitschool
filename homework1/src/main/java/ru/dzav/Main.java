package ru.dzav;

import ru.dzav.config.InitialInstantiationOfKotiks;
import ru.dzav.dao.KotikDAO;
import ru.dzav.dao.impl.KotikDAOImpl;
import ru.dzav.service.KotikService;
import ru.dzav.service.PlaygroundService;
import ru.dzav.service.impl.KotikServiceImpl;
import ru.dzav.service.impl.PlaygroundServiceImpl;
import ru.dzav.service.mapper.RequestAddKotikMapper;
import ru.dzav.service.mapper.ShowAllInfoMapper;
import ru.dzav.view.View;

public class Main {
    public static void main(String[] args) {

        InitialInstantiationOfKotiks init = new InitialInstantiationOfKotiks();
        KotikDAO kotikDAO = new KotikDAOImpl(init);

        ShowAllInfoMapper showAllInfoMapper = new ShowAllInfoMapper();
        RequestAddKotikMapper requestAddKotikMapper = new RequestAddKotikMapper();

        KotikService kotikService = new KotikServiceImpl(kotikDAO);
        PlaygroundService playgroundServiceImpl = new PlaygroundServiceImpl(kotikDAO, showAllInfoMapper, requestAddKotikMapper);

        View view = new View(playgroundServiceImpl, kotikService);
        view.showMenu();
    }

}



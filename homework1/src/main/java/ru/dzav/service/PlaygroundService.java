package ru.dzav.service;

import ru.dzav.model.dto.FullInfoKotikDTO;

import java.util.List;

public interface PlaygroundService {

    int getAmount();

    List<FullInfoKotikDTO> findAll();

    FullInfoKotikDTO showInfo(String kotikUUID);

    String findOne(String serialOfCat);

    boolean equalTwoKotiks(String kotikUUID1, String kotikUUID2);

    boolean add(String nameOfCat, String weightOfCat, String prettinessOfCat, String meow);

}

package ru.dzav.exception;

public class InvalidArgumentForParse extends RuntimeException {

    public InvalidArgumentForParse(String i) {
        System.err.println("Parameter is illegal. Try type a number and it should NOT have letters in it. What you typed - " + i);
    }
}

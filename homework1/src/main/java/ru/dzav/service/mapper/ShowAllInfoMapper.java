package ru.dzav.service.mapper;

import ru.dzav.model.Kotik;
import ru.dzav.model.dto.FullInfoKotikDTO;

public class ShowAllInfoMapper {

    public FullInfoKotikDTO showFullInfoRequest(Kotik kotik) {
        FullInfoKotikDTO fullInfoKotikDTO = new FullInfoKotikDTO();
        fullInfoKotikDTO.setName(kotik.getName());
        fullInfoKotikDTO.setWeight(kotik.getWeight());
        fullInfoKotikDTO.setFullness(kotik.getFullness());
        fullInfoKotikDTO.setPrettiness(kotik.getPrettiness());
        fullInfoKotikDTO.setMeow(kotik.getMeow());
        return fullInfoKotikDTO;
    }
}

package ru.dzav.dao;

import ru.dzav.model.Kotik;

import java.util.List;

public interface KotikDAO {

    void decreaseFullness(int i, String uuidKotik);

    int checkFullness(String kotikUUID);

    boolean addCat(Kotik kotik);

    void increaseFullness(String uuidKotik, int satiety);

    int getAmount();

    List<Kotik> getAll();

    Kotik getOne(int i);

    Kotik getOne(String kotikUUID);
}

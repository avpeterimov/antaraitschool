package ru.dzav.config;

import ru.dzav.model.Kotik;

import java.util.HashMap;
import java.util.Map;

public class InitialInstantiationOfKotiks {

    private final Map<String, Kotik> mapInit;

    public InitialInstantiationOfKotiks() {
        mapInit = new HashMap<>();
        Kotik kotik1 = new Kotik("Fursik", 25, 1000, "mmeeooww");
        mapInit.put(kotik1.getKotikUUID(), kotik1);

        Kotik kotik2 = new Kotik();
        kotik2.setName("Boris");
        kotik2.setWeight(22);
        kotik2.setPrettiness(1001);
        kotik2.setMeow("Way");
        mapInit.put(kotik2.getKotikUUID(), kotik2);
    }

    public Map<String, Kotik> getMapInit() {
        return mapInit;
    }
}

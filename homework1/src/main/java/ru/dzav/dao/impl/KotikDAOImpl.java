package ru.dzav.dao.impl;

import ru.dzav.config.InitialInstantiationOfKotiks;
import ru.dzav.dao.KotikDAO;
import ru.dzav.model.Kotik;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class KotikDAOImpl implements KotikDAO {
    private final Map<String, Kotik> hashMapCats;

    public KotikDAOImpl(InitialInstantiationOfKotiks initialInstantiationOfKotiks) {
        hashMapCats = initialInstantiationOfKotiks.getMapInit();
    }

    public void decreaseFullness(int i, String uuidKotik) {
        Kotik kotik = getOne(uuidKotik);
        kotik.setFullness(kotik.getFullness() - i);
    }

    public int checkFullness(String kotikUUID) {
        Kotik kotik = getOne(kotikUUID);
        return kotik.getFullness();
    }

    public boolean addCat(Kotik kotik) {
        hashMapCats.put(kotik.getKotikUUID(), kotik);
        return true;
    }

    public void increaseFullness(String uuidKotik, int satiety) {
        Kotik kotik = getOne(uuidKotik);
        kotik.setFullness(kotik.getFullness() + satiety);
    }

    public int getAmount() {
        return Kotik.getAmountOfCats();
    }

    public List<Kotik> getAll() {
        List<Kotik> list = new ArrayList<>();
        for (String entry : hashMapCats.keySet()) {
            Kotik kotik = hashMapCats.get(entry);
            list.add(kotik);
        }
        return list;
    }

    public Kotik getOne(int i) {
        int counter = 1;
        for (String s : hashMapCats.keySet()) {
            if (counter == i) {
                return hashMapCats.get(s);
            }
            counter++;
        }
        return null;
    }

    public Kotik getOne(String kotikUUID) {
        return hashMapCats.get(kotikUUID);

    }
}

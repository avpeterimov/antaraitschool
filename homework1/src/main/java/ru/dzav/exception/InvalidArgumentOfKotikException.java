package ru.dzav.exception;

public class InvalidArgumentOfKotikException extends RuntimeException {

    /**
     * Constructs a new {@code InvalidArgumentException} class with an
     * argument indicating the illegal name.
     *
     * @param name       the illegal name.
     * @param weight     the illegal weight.
     * @param prettiness the illegal prettiness.
     * @param meow       the illegal meow sound
     */
    public InvalidArgumentOfKotikException(String name, int weight, int prettiness, String meow) {
        System.err.println("Some parameters are illegal. Check them. Name - \"" + name + "\"" + "\n" +
                "weight should have only numbers in it. What you inputted - \"" + weight + "\"" + "\n" +
                "prettiness should have only numbers in it. What you inputted - \"" + prettiness + "\"" + "\n" +
                "Meow  - \"" + meow + "\"");
    }

    public InvalidArgumentOfKotikException(String i) {
        System.err.println("Parameter is illegal. Try type a number and it should NOT be more amount of cats or less 0. What you typed - " + i);
    }
}

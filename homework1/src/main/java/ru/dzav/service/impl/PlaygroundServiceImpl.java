package ru.dzav.service.impl;

import ru.dzav.dao.KotikDAO;
import ru.dzav.exception.InvalidArgumentOfKotikException;
import ru.dzav.model.Kotik;
import ru.dzav.model.dto.FullInfoKotikDTO;
import ru.dzav.service.PlaygroundService;
import ru.dzav.service.mapper.RequestAddKotikMapper;
import ru.dzav.service.mapper.ShowAllInfoMapper;

import java.util.ArrayList;
import java.util.List;

import static ru.dzav.service.Util.Util.parseStringToInt;

public class PlaygroundServiceImpl implements PlaygroundService {

    private final KotikDAO kotikDAO;
    private final ShowAllInfoMapper showAllInfoMapper;
    private final RequestAddKotikMapper requestAddKotikMapper;

    public PlaygroundServiceImpl(KotikDAO kotikDAO, ShowAllInfoMapper showAllInfoMapper, RequestAddKotikMapper requestAddKotikMapper) {
        this.kotikDAO = kotikDAO;
        this.showAllInfoMapper = showAllInfoMapper;
        this.requestAddKotikMapper = requestAddKotikMapper;
    }

    public FullInfoKotikDTO showInfo(String kotikUUID) {
        Kotik kotik = kotikDAO.getOne(kotikUUID);
        return showAllInfoMapper.showFullInfoRequest(kotik);
    }

    public String findOne(String serialOfCat) {
        try {
            Kotik kotik = kotikDAO.getOne(parseStringToInt(serialOfCat));
            return kotik.getKotikUUID();
        } catch (NullPointerException e) {
            throw new InvalidArgumentOfKotikException(serialOfCat);
        }
    }

    public boolean equalTwoKotiks(String kotikUUID1, String kotikUUID2) {
        Kotik kotik1 = kotikDAO.getOne(kotikUUID1);
        Kotik kotik2 = kotikDAO.getOne(kotikUUID2);
        return kotik1.equals(kotik2);
    }

    public boolean add(String nameOfCat, String weightOfCat, String prettinessOfCat, String meow) {
        try {
            return kotikDAO.addCat(requestAddKotikMapper.fromAddFormToKotik(nameOfCat, parseStringToInt(weightOfCat), parseStringToInt(prettinessOfCat), meow));
        } catch (InvalidArgumentOfKotikException e) {
            return false;
        }
    }

    public int getAmount() {
        return kotikDAO.getAmount();
    }

    public List<FullInfoKotikDTO> findAll() {
        List<Kotik> listKotiks = kotikDAO.getAll();
        List<FullInfoKotikDTO> listFullInfo = new ArrayList<>();
        for (Kotik kotik : listKotiks) {
            listFullInfo.add(showAllInfoMapper.showFullInfoRequest(kotik));
        }
        return listFullInfo;
    }

}

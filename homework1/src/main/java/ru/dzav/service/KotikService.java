package ru.dzav.service;

public interface KotikService {

    boolean chaseMouse(String kotikUUID);

    boolean play(String kotikUUID);

    boolean sleep(String kotikUUID);

    boolean stroll(String kotikUUID);

    boolean fight(String kotikUUID);

    boolean eat(String kotikUUID, String satietyUnits);

    boolean eat(String kotikUUID, String satiety, String nameOfFood);

    boolean eat(String kotikUUID);





}

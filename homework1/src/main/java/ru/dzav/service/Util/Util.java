package ru.dzav.service.Util;

import ru.dzav.exception.InvalidArgumentForParse;

public class Util {

    public static int parseStringToInt(String parsedString) {
        try {
            return Integer.parseInt(parsedString.trim());
        } catch (NumberFormatException e) {
            throw new InvalidArgumentForParse(parsedString);
        }
    }
}

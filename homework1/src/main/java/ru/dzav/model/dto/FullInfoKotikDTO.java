package ru.dzav.model.dto;

public class FullInfoKotikDTO {
    private String name;
    private int weight;
    private int fullness;
    private int prettiness;
    private String meow;

    public FullInfoKotikDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getFullness() {
        return fullness;
    }

    public void setFullness(int fullness) {
        this.fullness = fullness;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    @Override
    public String toString() {
        return "name = " + name + "; weight = " + weight + "; fullness=" + fullness + "; prettiness = " + prettiness + "; meow = " + meow;
    }

}

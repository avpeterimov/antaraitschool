package ru.dzav.service.mapper;

import ru.dzav.model.Kotik;

public class RequestAddKotikMapper {

    public Kotik fromAddFormToKotik(String name, int weight, int prettiness, String meowVoiceLine) {
        Kotik kotik = new Kotik();
        kotik.setName(name);
        kotik.setWeight(weight);
        kotik.setPrettiness(prettiness);
        kotik.setMeow(meowVoiceLine);
        return kotik;
    }
}

package ru.dzav.view;

import ru.dzav.exception.InvalidArgumentForParse;
import ru.dzav.exception.InvalidArgumentOfKotikException;
import ru.dzav.model.dto.FullInfoKotikDTO;
import ru.dzav.service.KotikService;
import ru.dzav.service.PlaygroundService;

import java.io.Console;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class View {

    private final PlaygroundService playgroundService;
    private final KotikService kotikService;

    public View(PlaygroundService playgroundService, KotikService kotikService) {
        this.playgroundService = playgroundService;
        this.kotikService = kotikService;
    }

    private static final String SEPARATOR = " - ";
    private static final String LINE_SEPARATOR = "\n";

    private static final String ACTION_SHOW_AMOUNT = "1";
    private static final String ACTION_ADD_CAT = "2";
    private static final String ACTION_SHOW_ALL_CATS = "3";
    private static final String ACTION_EQUALS_CAT = "4";
    private static final String ACTION_CHOOSE_CAT = "5";
    private static final String ACTION_WATCH = "6";

    private static final String ACTION_EXIT = "9";

    private static final String MESSAGE_SHOW_AMOUNT = "Show amout of cats";
    private static final String MESSAGE_ADD_CAT = "Add cat";
    private static final String MESSAGE_SHOW_ALL_CATS = "Show info about all cats";
    private static final String MESSAGE_CHOOSE_CAT_TO_COMPARE = "Choose 1-st and 2-nd kotik by typing their serial";
    private static final String MESSAGE_CHOOSE_CAT = "Choose cat by typing his serial";
    private static final String MESSAGE_WATCH = "Go watch cat";
    private static final String MESSAGE_EXIT = "Exit program";


    private static final String MAIN_MENU = ACTION_SHOW_AMOUNT + SEPARATOR + MESSAGE_SHOW_AMOUNT + LINE_SEPARATOR +
            ACTION_ADD_CAT + SEPARATOR + MESSAGE_ADD_CAT + LINE_SEPARATOR +
            ACTION_SHOW_ALL_CATS + SEPARATOR + MESSAGE_SHOW_ALL_CATS + LINE_SEPARATOR +
            ACTION_EQUALS_CAT + SEPARATOR + MESSAGE_CHOOSE_CAT_TO_COMPARE + LINE_SEPARATOR +
            ACTION_CHOOSE_CAT + SEPARATOR + MESSAGE_CHOOSE_CAT + LINE_SEPARATOR +
            ACTION_WATCH + SEPARATOR + MESSAGE_WATCH + LINE_SEPARATOR +
            ACTION_EXIT + SEPARATOR + MESSAGE_EXIT;
    private static final String MESSAGE_PRINT_NAME = "Print name";
    private static final String MESSAGE_PRINT_WEIGHT = "Print weight";
    private static final String MESSAGE_PRINT_PRETTINESS = "Print prettiness";
    private static final String MESSAGE_PRINT_MEOW = "Print meow voice line";
    private static final String CHOOSE_CAT = "Type the number of the cat you want to watch";
    private static final String MESSAGE_TYPE_SERIAL = "Type serial of Kotik";

    private static final String MESSAGE_WRONG_COMMAND = "Command you typed doesn't exists";

    Scanner scanner;

    public void showMenu() {


        String kotikUUID = "";
        while (true) {
            try {
                switch (inputInviter(MAIN_MENU)) {
                    case "0":

                        break;
                    case ACTION_SHOW_AMOUNT:
                        System.out.println(playgroundService.getAmount());
                        break;

                    case ACTION_ADD_CAT:
                        String name = inputInviter(MESSAGE_PRINT_NAME);
                        String weight = inputInviter(MESSAGE_PRINT_WEIGHT);
                        String prettiness = inputInviter(MESSAGE_PRINT_PRETTINESS);
                        String meow = inputInviter(MESSAGE_PRINT_MEOW);

                        playgroundService.add(name, weight, prettiness, meow);
                        break;

                    case ACTION_SHOW_ALL_CATS:
                        List<FullInfoKotikDTO> listKotiks = playgroundService.findAll();
                        int counter = 0;
                        for (FullInfoKotikDTO fullInfoKotikDTO : listKotiks) {
                            counter++;
                            System.out.println(counter + SEPARATOR + fullInfoKotikDTO);
                        }
                        System.out.println();
                        break;

                    case ACTION_EQUALS_CAT:
                        String kotikUUID1 = playgroundService.findOne(inputInviter(MESSAGE_TYPE_SERIAL));
                        String kotikUUID2 = playgroundService.findOne(inputInviter(MESSAGE_TYPE_SERIAL));

                        System.out.println(playgroundService.equalTwoKotiks(kotikUUID1, kotikUUID2));
                        break;

                    case ACTION_CHOOSE_CAT:
                        System.out.println(CHOOSE_CAT);
                        kotikUUID = playgroundService.findOne(inputInviter(MESSAGE_TYPE_SERIAL));
                        liveAnotherDay(kotikUUID);
                        System.out.println(playgroundService.showInfo(kotikUUID));
                        break;
                    case ACTION_WATCH:
                        if (!kotikUUID.equals("")) {
                            observe(kotikUUID);
                        } else {
                            System.out.println(CHOOSE_CAT);
                        }
                        break;

                    case ACTION_EXIT:
                        System.out.println(MESSAGE_EXIT);
                        System.exit(0);

                    default:
                        System.out.println(MESSAGE_WRONG_COMMAND);
                }
            } catch (InvalidArgumentForParse | InvalidArgumentOfKotikException e) {
                System.err.println(e);
            }
        }
    }

    private static final String ACTION_CHASING_MOUSE = "1";
    private static final String ACTION_PLAYING = "2";
    private static final String ACTION_SLEEPING = "3";
    private static final String ACTION_STROLLING = "4";
    private static final String ACTION_FIGHTING = "5";
    private static final String ACTION_EATING = "6";
    private static final String ACTION_LIVE_ONE_DAY = "7";

    private static final String ACTION_RETURN_TO_MAIN_MENU = "8";

    private static final String MESSAGE_KOTIK_CHASING_MOUSE = "Kotik is chasing mouse";
    private static final String MESSAGE_KOTIK_PLAYING = "Kotik is playing";
    private static final String MESSAGE_KOTIK_SLEEPING = "Kotik is sleeping";
    private static final String MESSAGE_KOTIK_STROLLING = "Kotik is strolling";
    private static final String MESSAGE_KOTIK_FIGHTING = "Kotik is fighting";
    private static final String MESSAGE_KOTIK_NEED_FOOD = "Kotik need to eat";
    private static final String MESSAGE_KOTIK_EATING = "Kotik is eating";
    private static final String MESSAGE_KOTIK_LIVING = "See one day of kotik";


    private static final String ACTION_MENU = ACTION_CHASING_MOUSE + SEPARATOR + MESSAGE_KOTIK_CHASING_MOUSE + LINE_SEPARATOR +
            ACTION_PLAYING + SEPARATOR + MESSAGE_KOTIK_PLAYING + LINE_SEPARATOR +
            ACTION_SLEEPING + SEPARATOR + MESSAGE_KOTIK_SLEEPING + LINE_SEPARATOR +
            ACTION_STROLLING + SEPARATOR + MESSAGE_KOTIK_STROLLING + LINE_SEPARATOR +
            ACTION_FIGHTING + SEPARATOR + MESSAGE_KOTIK_FIGHTING + LINE_SEPARATOR +
            ACTION_EATING + SEPARATOR + MESSAGE_KOTIK_EATING + LINE_SEPARATOR +
            ACTION_LIVE_ONE_DAY + SEPARATOR + MESSAGE_KOTIK_LIVING;

    private static final String MESSAGE_CHOSE_EAT = "Choose what kotik will eat";

    private static final String MESSAGE_CHOSE_FOOD_WITHOUT_ARGS = "Not specified ordinary food";
    private static final String MESSAGE_CHOSE_FOOD_SATIETY = "Food with specified satiety";
    private static final String MESSAGE_CHOSE_FOOD_SATIETY_AND_FOOD_NAME = "Specified food with specified name";

    private static final String ACTION_FOOD_ARGUMENTLESS = "1";
    private static final String ACTION_FOOD_SATIETY = "2";
    private static final String ACTION_FOOD_SATIETY_AND_FOOD_NAME = "3";

    private static final String MENU_FOR_FOOD = MESSAGE_CHOSE_EAT + LINE_SEPARATOR +
            ACTION_FOOD_ARGUMENTLESS + SEPARATOR + MESSAGE_CHOSE_FOOD_WITHOUT_ARGS + LINE_SEPARATOR +
            ACTION_FOOD_SATIETY + SEPARATOR + MESSAGE_CHOSE_FOOD_SATIETY + LINE_SEPARATOR +
            ACTION_FOOD_SATIETY_AND_FOOD_NAME + SEPARATOR + MESSAGE_CHOSE_FOOD_SATIETY_AND_FOOD_NAME;

    private static final String MESSAGE_SPECIFY_SATIETY = "Specify satiety of food";
    private static final String MESSAGE_SPECIFY_NAME = "Specify name of food";


    public void observe(String kotikUUID) {
        while (true) {

            System.out.println(playgroundService.showInfo(kotikUUID));
            try {
                switch (inputInviter(ACTION_MENU + LINE_SEPARATOR)) {
                    case ACTION_CHASING_MOUSE:
                        if (kotikService.chaseMouse(kotikUUID)) {
                            System.out.println(MESSAGE_KOTIK_CHASING_MOUSE);
                        } else {
                            System.out.println(MESSAGE_KOTIK_NEED_FOOD);
                        }
                        break;

                    case ACTION_PLAYING:
                        if (kotikService.play(kotikUUID)) {
                            System.out.println(MESSAGE_KOTIK_PLAYING);
                        } else {
                            System.out.println(MESSAGE_KOTIK_NEED_FOOD);
                        }

                        break;

                    case ACTION_SLEEPING:

                        if (kotikService.sleep(kotikUUID)) {
                            System.out.println(MESSAGE_KOTIK_SLEEPING);
                        } else {
                            System.out.println(MESSAGE_KOTIK_NEED_FOOD);
                        }
                        break;

                    case ACTION_STROLLING:
                        if (kotikService.stroll(kotikUUID)) {
                            System.out.println(MESSAGE_KOTIK_STROLLING);
                        } else {
                            System.out.println(MESSAGE_KOTIK_NEED_FOOD);
                        }
                        break;

                    case ACTION_FIGHTING:
                        if (kotikService.fight(kotikUUID)) {
                            System.out.println(MESSAGE_KOTIK_FIGHTING);
                        } else {
                            System.out.println(MESSAGE_KOTIK_NEED_FOOD);
                        }
                        break;

                    case ACTION_EATING:

                        switch (inputInviter(MENU_FOR_FOOD)) {
                            case ACTION_FOOD_ARGUMENTLESS:
                                kotikService.eat(kotikUUID);
                                break;

                            case ACTION_FOOD_SATIETY:
                                kotikService.eat(kotikUUID, inputInviter(MESSAGE_SPECIFY_SATIETY));
                                break;

                            case ACTION_FOOD_SATIETY_AND_FOOD_NAME:
                                kotikService.eat(kotikUUID, inputInviter(MESSAGE_SPECIFY_SATIETY), inputInviter(MESSAGE_SPECIFY_NAME));
                                break;

                        }
                        break;

                    case ACTION_LIVE_ONE_DAY:
                        liveAnotherDay(kotikUUID);
                        break;

                    case ACTION_RETURN_TO_MAIN_MENU:
                        showMenu();
                        break;

                    default:
                        System.out.println(MESSAGE_WRONG_COMMAND);
                }
            } catch (InvalidArgumentForParse | InvalidArgumentOfKotikException e) {
            }
        }
    }

    private String inputInviter(String message) {
        System.out.println(message);
        Console console = System.console();
        if (console != null) {
            try {
                return console.readLine();
            } catch (NoSuchElementException e) {
                System.out.println("Wow, how u did dat?"); //todo заглушка
            }
        } else {
            return getScanner().nextLine();
        }
        return null;
    }


    private Scanner getScanner() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        return scanner;
    }

    private void liveAnotherDay(String kotikUUID) { // это ужас, но я устал декомпозитровать и оно работает. -_-
        try {
            List<Method> listSorted = new ArrayList<>();

            for (Method method : kotikService.getClass().getDeclaredMethods()) {
                if (method.getParameterCount() == 1 && !method.getName().contains("checkFullness")) {
                    listSorted.add(method);
                }
            }

            for (int i = 1; i < 25; i++) {
                int randomInt = ThreadLocalRandom.current().nextInt(0, 5 + 1);

                Object ob = listSorted.get(randomInt).invoke(kotikService, kotikUUID);

                if (ob.toString().equals("false")) {
                    for (Method method : listSorted) {
                        if (method.getName().contains("eat")) {
                            method.invoke(kotikService, kotikUUID);
                            i++;
                            break;
                        }
                    }

                }
                System.out.println(i + SEPARATOR + ob);
            }

        } catch (InvocationTargetException | IllegalAccessException e) {
            System.out.println("Smth went wrong");
        }
    }
}
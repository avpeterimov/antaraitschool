package ru.dzav.service.impl;

import ru.dzav.dao.KotikDAO;
import ru.dzav.service.KotikService;
import ru.dzav.service.Util.Util;

public class KotikServiceImpl implements KotikService {

    private static final int TIRED_VALUE = 0;
    private static final int DEFAULT_LABOR_VALUE = 1;
    private static final String DEFAULT_FOOD_NAME = "Viskas";
    private static final String DEFAULT_FOOD_SATIETY = "3";

    private final KotikDAO kotikDAO;

    public KotikServiceImpl(KotikDAO kotikDAO) {
        this.kotikDAO = kotikDAO;
    }

    public boolean chaseMouse(String kotikUUID) {
        return checkFullness(kotikUUID);
    }

    public boolean play(String kotikUUID) {
        return checkFullness(kotikUUID);
    }

    public boolean sleep(String kotikUUID) {
        return checkFullness(kotikUUID);
    }

    public boolean stroll(String kotikUUID) {
        return checkFullness(kotikUUID);
    }

    public boolean fight(String kotikUUID) {
        return checkFullness(kotikUUID);
    }

    public boolean eat(String kotikUUID, String satietyUnits) {
        int satietyUnitsToInt = Util.parseStringToInt(satietyUnits);
        kotikDAO.increaseFullness(kotikUUID, satietyUnitsToInt);
        return true;
    }

    public boolean eat(String kotikUUID, String satietyUnits, String nameOfFood) {
        kotikDAO.increaseFullness(kotikUUID, Util.parseStringToInt(satietyUnits));
        return true;
    }

    public boolean eat(String kotikUUID) {
        eat(kotikUUID, DEFAULT_FOOD_SATIETY, DEFAULT_FOOD_NAME);
        return true;
    }

    private boolean checkFullness(String kotikUUID) {
        boolean status = kotikDAO.checkFullness(kotikUUID) > TIRED_VALUE;
        if (status) {
            kotikDAO.decreaseFullness(DEFAULT_LABOR_VALUE, kotikUUID);
        }
        return status;
    }
}
